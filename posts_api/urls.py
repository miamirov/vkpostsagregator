from django.conf.urls import url
from django.urls import path, include
from posts_api import views

urlpatterns = [
    path(r'groups/', views.GroupList.as_view(), name='groups'),
]

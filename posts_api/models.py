from django.db import models
from django.contrib.auth.models import User


# Create your models here.

class Group(models.Model):
    id = models.IntegerField(primary_key=True)
    users = models.ManyToManyField(User, related_name='vk_groups')


class Post(models.Model):
    text = models.TextField
    group = models.ForeignKey(Group, on_delete=models.CASCADE)
    id = models.BigIntegerField
    date = models.BigIntegerField


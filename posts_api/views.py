from django.http import HttpResponseRedirect
from django.shortcuts import render
from django.views.generic import FormView
from rest_framework.renderers import TemplateHTMLRenderer
from rest_framework.response import Response
from rest_framework.views import APIView

from posts_api.models import Group
from posts_api.forms import GroupForm

# Create your views here.


class GroupList(APIView):
    renderer_classes = [TemplateHTMLRenderer]
    template_name = 'groups.html'

    def get(self, request):
        form = GroupForm()
        query_set = request.user.vk_groups.all()

        return Response({'groups': query_set, 'add_form': form})

    def post(self, request):
        if 'delete' in request.POST:
            id = request.POST.get('id')
            group = Group.objects.get(id=id)
            group.users.remove(request.user)
        elif 'add' in request.POST:
            group_form = GroupForm(request.POST)
            if group_form.is_valid():
                group, created = Group.objects.get_or_create(id=group_form.cleaned_data['id'])
                group.users.add(request.user)
        form = GroupForm()
        query_set = request.user.vk_groups.all()

        return Response({'groups': query_set, 'add_form': form})









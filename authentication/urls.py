from django.conf.urls import url
from django.urls import path, include
from . import views

urlpatterns = [
    path(r'sign_up/', views.SignUp.as_view(), name='sign_up'),
    path('', include('django.contrib.auth.urls')),
]

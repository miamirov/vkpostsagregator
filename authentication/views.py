from django.shortcuts import render, redirect
from django.contrib.auth.forms import UserCreationForm

# Create your views here.
from rest_framework.renderers import TemplateHTMLRenderer
from rest_framework.views import APIView


class SignUp(APIView):
    renderer_classes = [TemplateHTMLRenderer]
    template_name = 'sign_up.html'

    def get(self, request):
        form = UserCreationForm()
        return render(request, 'registration/sign_up.html', {'form': form})

    def post(self, request):
        form = UserCreationForm(request.POST)
        if form.is_valid():
            form.save()
            return redirect('login')
        else:
            return redirect('sign_up')

